﻿using System;
using System.Linq;
using System.Web.Mvc;

namespace WebApp.Controllers
{
    public class OwnershipController : Controller
    {
        private readonly ShopsEntities _context;

        public OwnershipController()
        {
            _context = new ShopsEntities();
        }

        [HttpGet]
        public ActionResult Index()
        {
            return View(_context.Ownership.ToList());
        }

        [HttpGet]
        public ActionResult Insert()
        {
            ViewBag.Shops = _context.Shop.ToList();
            ViewBag.Owners = _context.Owner.ToList();

            return View();
        }

        [HttpPost]
        public ActionResult Insert(Ownership ownership)
        {
            try
            {
                _context.Ownership.Add(ownership);
                _context.SaveChanges();

                ViewBag.Message = "Успішно додано новий запис";
            }
            catch (Exception ex)
            {
                ViewBag.ShopId = ownership.shop_id;
                ViewBag.OwnerId = ownership.owner_id;
                ViewBag.Date = ownership.date;
                ViewBag.DepositAmount = ownership.deposit_amount;

                ViewBag.Message = "Некоректні дані";
            }
            finally
            {
                ViewBag.Shops = _context.Shop.ToList();
                ViewBag.Owners = _context.Owner.ToList();
            }

            return View();
        }

        [HttpGet]
        public ActionResult Edit(int? id)
        {
            Ownership ownership = _context.Ownership.Find(id);

            if (ownership == null)
            {
                return HttpNotFound();
            }

            ViewBag.Shops = _context.Shop.ToList();
            ViewBag.Owners = _context.Owner.ToList();

            return View(ownership);
        }

        [HttpPost]
        public ActionResult Edit(Ownership ownership)
        {
            try
            {
                Ownership ownershipEdit = _context.Ownership.Find(ownership.id);
                ownershipEdit.shop_id = ownership.shop_id;
                ownershipEdit.owner_id = ownership.owner_id;
                ownershipEdit.date = ownership.date;
                ownershipEdit.deposit_amount = ownership.deposit_amount;

                _context.SaveChanges();

                ViewBag.Message = "Дані успішно змінено";
            }
            catch (Exception ex)
            {
                ViewBag.Message = "Некоректні дані";
            }
            finally
            {
                ViewBag.Shops = _context.Shop.ToList();
                ViewBag.Owners = _context.Owner.ToList();
            }

            return View(ownership);
        }

        [HttpGet]
        public ActionResult Delete(int? id)
        {
            Ownership ownership = _context.Ownership.Find(id);

            if (ownership == null)
            {
                return HttpNotFound();
            }

            return View(ownership);
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            Ownership ownership = _context.Ownership.Find(id);

            if (ownership == null)
            {
                return HttpNotFound();
            }

            _context.Ownership.Remove(ownership);
            _context.SaveChanges();

            return RedirectToAction(nameof(Index));
        }
    }
}