﻿using System.Linq;
using System.Web.Mvc;

namespace WebApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly ShopsEntities _context;

        public HomeController()
        {
            _context = new ShopsEntities();
        }

        public ActionResult Index()
        {
            ViewBag.CountShop = _context.Shop.Count();
            ViewBag.CountOwner = _context.Owner.Count();
            ViewBag.CountOwnership = _context.Ownership.Count();

            return View();
        }
    }
}