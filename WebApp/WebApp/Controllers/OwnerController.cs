﻿using System;
using System.Linq;
using System.Web.Mvc;

namespace WebApp.Controllers
{
    public class OwnerController : Controller
    {
        private readonly ShopsEntities _context;

        public OwnerController()
        {
            _context = new ShopsEntities();
        }
        
        [HttpGet]
        public ActionResult Index()
        {
            return View(_context.Owner.ToList());
        }

        [HttpGet]
        public ActionResult Insert()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Insert(Owner owner)
        {
            try
            {
                _context.Owner.Add(owner);
                _context.SaveChanges();

                ViewBag.Message = "Успішно додано новий запис";
            }
            catch (Exception ex)
            {
                ViewBag.FullName = owner.full_name;
                ViewBag.Address = owner.address;
                ViewBag.RegionResidence = owner.region_residence;
                ViewBag.Birthday = owner.birthday;

                ViewBag.Message = "Некоректні дані";
            }

            return View();
        }

        [HttpGet]
        public ActionResult Edit(int? id)
        {
            Owner owner = _context.Owner.Find(id);

            if (owner == null)
            {
                return HttpNotFound();
            }

            return View(owner);
        }

        [HttpPost]
        public ActionResult Edit(Owner owner)
        {
            try
            {
                Owner ownerEdit = _context.Owner.Find(owner.id);
                ownerEdit.full_name = owner.full_name;
                ownerEdit.address = owner.address;
                ownerEdit.region_residence = owner.region_residence;
                ownerEdit.birthday = owner.birthday;

                _context.SaveChanges();

                ViewBag.Message = "Дані успішно змінено";
            }
            catch (Exception ex)
            {
                ViewBag.Message = "Некоректні дані";
            }

            return View(owner);
        }

        [HttpGet]
        public ActionResult Delete(int? id)
        {
            Owner owner = _context.Owner.Find(id);

            if (owner == null)
            {
                return HttpNotFound();
            }

            return View(owner);
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            Owner owner = _context.Owner.Find(id);

            if (owner == null)
            {
                return HttpNotFound();
            }

            _context.Owner.Remove(owner);
            _context.SaveChanges();

            return RedirectToAction(nameof(Index));
        }
    }
}