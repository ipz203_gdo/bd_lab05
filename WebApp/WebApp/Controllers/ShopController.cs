﻿using System;
using System.Linq;
using System.Web.Mvc;

namespace WebApp.Controllers
{
    public class ShopController : Controller
    {
        private readonly ShopsEntities _context;

        public ShopController()
        {
            _context = new ShopsEntities();
        }

        [HttpGet]
        public ActionResult Index()
        {
            return View(_context.Shop.ToList());
        }

        [HttpGet]
        public ActionResult Insert()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Insert(Shop shop)
        {
            try
            {
                _context.Shop.Add(shop);
                _context.SaveChanges();

                ViewBag.Message = "Успішно додано новий запис";
            }
            catch (Exception ex)
            {
                ViewBag.Name = shop.name;
                ViewBag.Region = shop.region;
                ViewBag.Profile = shop.profile;
                ViewBag.Capital = shop.capital;

                ViewBag.Message = "Некоректні дані";
            }

            return View();
        }

        [HttpGet]
        public ActionResult Edit(int? id)
        {
            Shop shop = _context.Shop.Find(id);

            if (shop == null)
            {
                return HttpNotFound();
            }

            return View(shop);
        }

        [HttpPost]
        public ActionResult Edit(Shop shop)
        {
            try
            {
                Shop shopEdit = _context.Shop.Find(shop.id);
                shopEdit.name = shop.name;
                shopEdit.region = shop.region;
                shopEdit.profile = shop.profile;
                shopEdit.capital = shop.capital;

                _context.SaveChanges();

                ViewBag.Message = "Дані успішно змінено";
            }
            catch (Exception ex)
            {
                ViewBag.Message = "Некоректні дані";
            }

            return View(shop);
        }

        [HttpGet]
        public ActionResult Delete(int? id)
        {
            Shop shop = _context.Shop.Find(id);

            if (shop == null)
            {
                return HttpNotFound();
            }

            return View(shop);
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            Shop shop = _context.Shop.Find(id);

            if (shop == null)
            {
                return HttpNotFound();
            }

            _context.Shop.Remove(shop);
            _context.SaveChanges();

            return RedirectToAction(nameof(Index));
        }
    }
}